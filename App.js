//Packages
import React from 'react'
import { Dimensions, Animated, Easing } from 'react-native'
import { Root } from "native-base";
import { createDrawerNavigator, createStackNavigator, createAppContainer } from "react-navigation"
//import Loader from 'react-native-mask-loader';
import SplashScreen from 'react-native-splash-screen'
import { createStore } from 'redux';
import { Provider } from 'react-redux';
//import os from 'react-native-os';

//Components
import DrawerContent from '@Component/Menu/Left'
import InitialScreen from '@Screen/InitialScreen'
import allReducers from '@Reducer/index.js';
import NavigationService from '@Service/Navigation';
import ConfigurationView from '@Screen/Configuration';
import BusinessPlaceView from '@Screen/BusinessPlaceView';
import Dashboard from '@Screen/Dashboard';
import Login from '@Screen/Login';
import ForgotPassword from '@Screen/ForgotPassword';
import Customer from '@Screen/BusinessPartner/Customer';
import TabsCustomer from '@Screen/BusinessPartner/Customer/Tabs/router';
import CustomerDetail from '@Screen/BusinessPartner/Customer/Tabs/CustomerDetail';
import CustomerCreate from '@Screen/BusinessPartner/Customer/CustomerCreate';

//import '@Database/index';
import 'reflect-metadata';
import { createConnection, getRepository } from "typeorm";
import { Cliente } from '@Database/Entity/Cliente';
import { User } from '@Database/Entity/User';
import { Configuration } from '@Database/Entity/Configuration';
import { BusinessPlace } from '@Database/Entity/BusinessPlace';
import { Synchronization } from '@Database/Entity/Synchronization';

//const path = require("path");
//const path = os.homedir();
var dirName = require('NativeModules').DirName;
const store = createStore(allReducers);
const deviceWidth = Dimensions.get("window").width;

const Drawer = createDrawerNavigator(
  {
    TabsCustomer: {
      screen: TabsCustomer
    },
    Customer: {
      screen: Customer
    },
    ConfigurationView: {
      screen: ConfigurationView
    },
    BusinessPlaceView: {
      screen: BusinessPlaceView
    },
    Dashboard: {
      screen: Dashboard
    }
  },
  {
    contentComponent: DrawerContent,
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    headerMode: "none",
    initialRouteName: "Dashboard",
    drawerWidth: deviceWidth - 50
  }
)

const AppNav = createStackNavigator(
  {
    CustomerCreate: {
      screen: CustomerCreate
    },
    CustomerDetail: {
      screen: CustomerDetail
    },
    Customer: {
      screen: Customer
    },
    ConfigurationView: {
      screen: ConfigurationView
    },
    BusinessPlaceView: {
      screen: BusinessPlaceView
    },
    Login: {
      screen: Login
    },
    ForgotPassword: {
      screen: ForgotPassword
    },
    InitialScreen: {
      screen: InitialScreen
    },
    Drawer: {
      screen: Drawer
    }
  },
  {
    headerMode: "none",
    initialRouteName: 'InitialScreen'/*'InitialScreen'*/,
    //Remove transicao de tela, efeitos
    transitionConfig : () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
      },
    }),
  }
)
console.disableYellowBox = true;

const AppContainer = createAppContainer(AppNav);

export default class App extends React.Component {
  state = {
    appReady: false,
    rootKey: Math.random(),
  };

  constructor() {
    super();
    //alert(path);
  }

  componentDidMount() {
    SplashScreen.hide();
    this.resetAnimation();
  }

  resetAnimation() {
    this.setState({
      appReady: false,
      rootKey: Math.random()
    });

    setTimeout(() => {
      this.setState({
        appReady: true,
      });
    }, 2000);
  }
  render() {
    return (
      <Provider store={store}>
        <Root>
          {/*<Loader
          isLoaded={this.state.appReady}
          imageSource={require('./assets/images/music_6x.png')}
          backgroundStyle={{ backgroundColor: "#DF3A62" }}
        >
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Loader>
        */}
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Root>
      </Provider>
    );
  }
}