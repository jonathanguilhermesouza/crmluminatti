import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class BusinessPlace {

    @PrimaryColumn("int")
    BPLID: number;

    @Column("varchar", {
        length: 100
    })
    BPLName: string;

    @Column("varchar", {
        length: 100
    })
    BPLNameForeign: string;
}