import {Entity, Column, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import { User } from "@Database/Entity/User";

@Entity()
export class Synchronization {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("datetime")
    dateTime: date;

    @Column("boolean")
    wasSucesso: boolean

    @ManyToOne(type => User, user => user.synchronizations)
    user: User;
}