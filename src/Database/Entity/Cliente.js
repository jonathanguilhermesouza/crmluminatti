import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Cliente {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar",{
        length: 100
    })
    nome: string;

    @Column("int")
    idade: number;

}