
module.exports = {

  themeColor:"#39b6ec",
  defaultTextColor:"#585858",
  defaultLightTextColor:"#fff",
  defaultThemeTextColor:"#39b6ec",
  headerColor: "#313d3f",
  dangerColor: "#FF0000",
  
  get getDefaultTextColor() {
    return this.defaultTextColor;
  },

  get getDefaultLightTextColor() {
    return this.defaultLightTextColor;
  },

  get getDefaultThemeTextColor() {
    return this.defaultThemeTextColor;
  },

  get getThemeColor() {
    return this.themeColor;
  },

  get getHeaderColor() {
    return this.headerColor;
  },

  get getDangerColor() {
    return this.dangerColor;
  }
};
