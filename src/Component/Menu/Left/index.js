import React, { Component } from "react";
import { Image, AsyncStorage } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  View,
} from "native-base";
import styles from "./Style";
import NavigationService from '@Service/Navigation';
import AccountService from '@Service/AccountService';

const drawerImage = require("@Asset/images/avatar-generic.jpg");
let AccountServiceInstance = new AccountService();


const logout = () => {
  AccountServiceInstance.logout()
    .then((data) => {
      NavigationService.navigate("Login");
    })
    .catch((error) => {

    })
};

class MenuLeft extends Component {

  datas1 = [
    {
      name: "Dashboard",
      route: "Dashboard",
      description: "Indicadores",
      icon: "tachometer",
    },
    {
      name: "Pedidos",
      route: "PublicHome",
      description: "Lançar e gerenciar pedidos",
      icon: "shopping-bag",
    },
    {
      name: "Clientes",
      route: "Customer",
      description: "Lista de clientes com seus históricos",
      icon: "group",
    },
    {
      name: "Estoque",
      route: "Customer",
      description: "Produtos e estoques",
      icon: "archive",
    },
    /*{
      name: "Relatórios",
      route: "PublicPropertySearch",
      description: "Extrair dados do dispositivo e da nuvem",
      icon: "bar-chart",
    },*/
    /*{
      name: "Informações",
      route: "PublicAgents",
      description: "Informações sobre o sistema",
      icon: "info-circle",
    },*/
    {
      name: "Sincronização online",
      route: "Customer",
      description: "Envia e recebe dados atualizados",
      icon: "refresh",
    },
    /*{
      name: "Aplicativo na nuvem",
      route: "PublicAboutUs",
      description: "Acesso ao sistema web",
      icon: "cloud"
    },*/
    {
      name: "Configurações",
      route: "Configuration",
      description: "Parãmetros de acesso",
      icon: "cogs"
    },
  ];
  datas2 = [
    /*{
      name: "Sign In",
      route: "Login",
      icon: "login-variant",
      description: "",
      type: "MaterialCommunityIcons",
    },*/
    {
      name: "Troca de filial",
      route: "BusinessPlaceView",
      description: "Lista de filiais",
      icon: "building",
    },
    {
      name: "Log off",
      route: "Login",
      icon: "logout",
      description: "",
      isFunction: true,
      function: "logout()",
      type: "MaterialCommunityIcons",
    }
  ];


  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      usernameLogged: ''
    };
  }

  componentDidMount() {
    this.getUserLogged();
  }

  getUserLogged() {
    AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
      if (result !== null) {
        let basicDataUserLogged = JSON.parse(result);
        this.setState({ usernameLogged: basicDataUserLogged.username });
      }
    });
  }

  /*logout() {
    alert("deslogando em 3 2 1...");
  }*/

  renderList(datas) {
    return (
      <List
        dataArray={datas}
        renderRow={data =>
          <ListItem
            button
            noBorder
            onPress={() => data.isFunction ? eval(data.function) : NavigationService.navigate(data.route)}
          >
            <Left>
              <Icon
                active
                name={data.icon}
                style={{ color: "#999", fontSize: 24, width: 30 }}
                type={data.type || 'FontAwesome'}
              />
              <View style={{ flexDirection: "column" }}>
                <Text style={styles.text}>
                  {data.name}
                </Text>
                <Text style={[styles.text, { fontWeight: "100", color: "#999" }]}>
                  {data.description}
                </Text>
              </View>
            </Left>
            {
              data.types &&
              <Right style={{ flex: 1 }}>
                <Badge>
                  <Text
                    style={styles.badgeText}
                  >{`${data.types}`}</Text>
                </Badge>
              </Right>
            }
          </ListItem>}
      />
    )
  }
  render() {
    return (
      <Container>
        <Content>
          <View style={styles.drawerCover}>
            <Image square style={styles.drawerImage} source={drawerImage} />
            <Text style={styles.drawerText}>Bem-vindo {this.state.usernameLogged}!</Text>
          </View>
          <View style={styles.divider}>
            {this.renderList(this.datas1)}
          </View>
          <View>
            {this.renderList(this.datas2)}
          </View>

        </Content>
      </Container>
    );
  }
}

export default MenuLeft;
