import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import CommonColor from '@Theme/Variables/CommonColor';

const InputField = ({
    secureTextEntry,
    textColor,
    autoCapitalize,
    onTouchStart,
    keyboardType,
    tintColor,
    baseColor,
    name,
    label,        // field name - required
    customStyle,
    onChangeText,   // event
    value,          // field value
    disabled,
    placeholder,
    errors,         // this array prop is automatically passed down to this component from <Form />
}) => {
    return (
        <View>

            <TextField
                onTouchStart={onTouchStart}
                keyboardType={keyboardType}
                autoCapitalize={autoCapitalize}
                textColor={textColor}
                tintColor={tintColor}
                baseColor={baseColor}
                label={label}
                value={value ? value : ""}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder ? placeholder : ""}
                disabled={disabled}
                onChangeText={onChangeText ? (val) => onChangeText(val) : null}
                style={customStyle ? customStyle : {}}
            />

            {errors && errors.length > 0 && errors.map((item, index) =>
                item.field === name && item.error ?
                    <Text style={styles.formErrorText}>
                        {item.error}
                    </Text>
                    : <View />
            )
            }
        </View>
    );
}

export default InputField;

const styles = StyleSheet.create({
    formErrorText: {
        fontSize: Platform.OS === "android" ? 12 : 15,
        color: CommonColor.getDangerColor,
        textAlign: "right",
        top: -10
    }
});