import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import CommonColor from '@Theme/Variables/CommonColor';

const InputDropdown = ({
    textColor,
    baseColor,
    name,
    label,
    onChangeText,
    value,
    disabled,
    placeholder,
    errors,
    selectedItemColor,
    containerStyle,
    data
}) => {
    return (
        <View>

            <Dropdown
                baseColor={baseColor}
                textColor={textColor}
                selectedItemColor={selectedItemColor}
                containerStyle={containerStyle}
                label={label}
                value={value && value}
                data={data}
                onChangeText={onChangeText ? (val) => onChangeText(val) : null}
            />

            {errors && errors.length > 0 && errors.map((item, index) =>
                item.field === name && item.error ?
                    <Text style={styles.formErrorText}>
                        {item.error}
                    </Text>
                    : <View />
            )
            }
        </View>
    );
}

export default InputDropdown;

const styles = StyleSheet.create({
    formErrorText: {
        fontSize: Platform.OS === "android" ? 12 : 15,
        color: CommonColor.getDangerColor,
        textAlign: "right",
        top: -10
    }
});