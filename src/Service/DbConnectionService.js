import 'reflect-metadata';
import { createConnection } from "typeorm";
import { Cliente } from '@Database/Entity/Cliente';
import { User } from '@Database/Entity/User';
import { Configuration } from '@Database/Entity/Configuration';
import { BusinessPlace } from '@Database/Entity/BusinessPlace';
import { Synchronization } from '@Database/Entity/Synchronization';

export default class DbConnectionService {

  async connection() {
    return new Promise((resolve, reject) => {
      createConnection({
        type: 'react-native',
        database: 'luminatti.sqlite3',
        location: 'default',
        logging: ['error', 'query', 'schema'],
        synchronize: true,
        entities: [
          Cliente,
          Configuration,
          BusinessPlace,
          User,
          Synchronization,
        ],
        logging: false
      }).then(connection => {
        resolve(connection);
      }).catch(error => {
        reject(error);
      });
    })
  };
}