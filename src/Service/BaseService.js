import Axios from "axios";
import Config from "@App/Config";
import {
    AsyncStorage
} from "react-native";

export class BaseService {
    baseRequest = {
        method: "", //POST, GET, PUT ETC
        headers: {
            "cache-control": "no-cache",
            "accept": "application/json, text/javascript, */*; q=0.01",
            "content-type": "application/json",
            "B1S-CaseInsensitive": true,
            //"cookie": "B1SESSION=ssfsfsdfsdfsdfsdfsdfsdfsd; ROUTEID=.node0"
            cookie: {}
        },
        url: "",
        data: {}
    };

    static axioInstance;
    baseUrl = Config.BASE_URL;
    servicesUrl = this.baseUrl + "b1s/v1/";
    conditionsUrl = "";

    constructor() {
        if (!this.axionsInstance)
            this.axioInstance = Axios.create();
    }

    getSessionId() {
        AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
            let jsonObj = JSON.parse(result);
            //jsonObj.sessionId = "a387ab32-521e-11e9-8000-000c291bfd4b";
            let cookie = jsonObj ? "B1SESSION=" + jsonObj.sessionId + "; ROUTEID=.node0" : "";
            this.baseRequest.headers.cookie = cookie;
        });
    }

    /**
    * ShortHand for post requests
    * @param {string} url 
    * @param {Object} data post data
    * @param {Object} axiosConfig Optional - custom object to axios request configuration
    * @returns {Promise<Response>} promise with json data
    */
    async put(url, data, axiosConfig) {
        return this.doRequest("PUT", url, data, axiosConfig);
    }

    /**
        * ShortHand for post requests
        * @param {string} url 
        * @param {Object} data post data
        * @param {Object} axiosConfig Optional - custom object to axios request configuration
        * @returns {Promise<Response>} promise with json data
        */
    async patch(url, data, axiosConfig) {
        return this.doRequest("PATCH", url, data, axiosConfig);
    }

    /**
        * ShortHand for post requests
        * @param {string} url 
        * @param {Object} data post data
        * @param {Object} axiosConfig Optional - custom object to axios request configuration
        * @returns {Promise<Response>} promise with json data
        */
    async post(url, data, axiosConfig) {
        return this.doRequest("POST", url, data, axiosConfig);
    }

    /**
    * ShortHand for GET requests
    * @param {string} url 
    * @returns {Promise<Response>} promise with json data
    */
    async get(url) {
        return await this.doRequest("GET", url, null)
    }

    /**
    * ShortHand for DELETE requests
    * @param {string} url 
    * @returns {Promise<Response>} promise with json data
    */
    async delete(url) {
        return await this.doRequest("DELETE", url, null)
    }

    async doRequest(method, url, data, axiosConfig) {
        this.getSessionId();
        console.log(url);
        var request = this.baseRequest;
        request.method = method || request.method;
        request.data = data;
        request.url = url;
        return this.axioInstance.request(request).then((response) => response.data);
    }
};