import { BaseService } from "./BaseService";

const entity = 'BusinessPartnerGroups';
export default class GroupCodeService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + entity + request.conditionsUrl);
  };
}
