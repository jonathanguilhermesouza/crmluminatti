import { NetInfo } from 'react-native';
import {
    BaseService
} from "./BaseService";

export default class NetworkService extends BaseService {
    static isConnected() {
        return NetInfo.getConnectionInfo().then((connectionInfo) => {
            return connectionInfo.type !== 'none';
        })
    };

    async isAuthenticated() {
        var responseAuthenticated = {
            IsAuthenticated: false,
            Message: "",
            IsError: false,
            DateTime: new Date()
        }
        return await this.post(this.servicesUrl + 'LicenseService_GetInstallationNumber', null)
            .then((response) => {
                responseAuthenticated.IsAuthenticated = true;
            })
            .catch((error) => {
                responseAuthenticated.IsError = true;
            })
            .finally((response) => {
                return responseAuthenticated.IsAuthenticated;
            })
    };
}