import { AsyncStorage } from "react-native";
import {
  BaseService
} from "./BaseService";

export default class AccountService extends BaseService {

  async login(loginRequest) {
    var loginResponse = {
      IsAuthenticated: false,
      Message: "",
      SessionId: "",
      SessionTimeout: 0,
      Version: "",
      DateTime: new Date()
    }
    return await this.post(this.servicesUrl + 'Login', loginRequest)
      .then((response) => {
        loginResponse.IsAuthenticated = true;
        loginResponse.SessionId = response.SessionId;
        loginResponse.Version = response.Version;
        loginResponse.SessionTimeout = response.SessionTimeout;
      })
      .catch((error) => {
        alert(JSON.stringify(error));
        loginResponse.IsAuthenticated = false;
        loginResponse.Message = error.message;
      })
      .finally((response) => {
        return loginResponse;
      })
  };

  loginOffline(loginRequest) {

    return new Promise((resolve, reject) => {

      if (!loginRequest)
        reject('loginRequest inválido');

      AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
        if (result !== null) {
          let basicDataUserLogged = JSON.parse(result);

          if (!basicDataUserLogged) {
            alert('Para "Login offline" você deve logar pelo menos uma vez online.');
            return;
          }
          resolve(loginRequest.UserName === basicDataUserLogged.username && loginRequest.Password === basicDataUserLogged.password);
        }
      });
    })
  };

  async logout() {
    return await this.post(this.servicesUrl + 'Logout', null)
      .then((response) => {

        AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
          if (result !== null) {
            let basicDataUserLogged = JSON.parse(result);
            basicDataUserLogged.sessionId = "";
            AsyncStorage.setItem('BASIC_DATA_USER_LOGGED', JSON.stringify(basicDataUserLogged));
          }
        });

      })
      .catch((error) => {
        //alert('erro deslogando');
      })
      .finally((response) => {
        return null;
      })
  };
}
