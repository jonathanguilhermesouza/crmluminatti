import { BaseService } from "./BaseService";

const entity = 'BusinessPartners';
export default class CustomerService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + entity + request.conditionsUrl);
  };

  async getById(request) {
    return await this.get(this.servicesUrl + entity + request.conditionsUrl);
  };

  async create(customer) {
    return await this.post(this.servicesUrl + entity, customer)
  };

  async edit(cardCode, customer) {
    return await this.put(this.servicesUrl + entity + `('${cardCode}')`, customer)
  };

  async remove(request) {
    return await this.delete(this.servicesUrl + entity + request.conditionsUrl);
  };
}
