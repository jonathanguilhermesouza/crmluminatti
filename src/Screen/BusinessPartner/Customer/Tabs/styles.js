import CommonColor from '@Theme/Variables/CommonColor';

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewTab: {
    marginBottom: Platform.OS === 'android' ? 0 : 10
  },
  iconTab: {
    alignSelf: 'center'
  },
  textTab: {
    color: '#fff',
    fontSize: 13,
    marginTop: 5
  },
  viewButton: {
    flexDirection: 'row',
    height: Platform.OS === 'android' ? 60 : 65,
    width: '100%',
    shadowOpacity: 0.75,
    shadowRadius: 2,
    shadowColor: '#bebebe',
    shadowOffset: { height: 0, width: 0 }
  },
  buttonTab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#00aff0",
    color: "#fff",
  }
}