const React = require("react-native");
const { Platform, Dimensions } = React;

import CommonColor from '@Theme/Variables/CommonColor';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    //justifyContent: "center",
    flexDirection: "column"
  },
  stepIndicator: {
    marginVertical: 50
  },
  page: {
    //flex: 1,
    paddingLeft: 20,
    paddingRight: 20
    //justifyContent: 'center',
    //alignItems: 'center'
  },
  stepLabel: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#999999'
  },
  stepLabelSelected: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#4aae4f'
  },
  stepIndicator: {
    marginVertical: 50
  },
  botomButton: {
    flex: 1,
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#1C1C1C',
    alignSelf: "flex-end"
  },
  textBotomButton: {
    fontSize: 20,
    color: "#fff"
  }
}