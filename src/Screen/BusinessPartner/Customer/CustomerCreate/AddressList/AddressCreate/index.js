//Packages
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, ScrollView, Alert } from 'react-native';
import { Form, Field } from 'react-native-validate-form';

//Components
import InputField from '@Component/Input/InputField';
import InputDropdown from '@Component/Input/InputDropdown';

//Services
import SisService from '@Service/SisService';

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';

const required = value => (value ? undefined : "Obrigatório");
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const addressTypeList = [{ value: 'Cobrança', id: 'B' }, { value: 'Entrega', id: 'S' }];
const baseColor = "#333";

var SisInstance = new SisService();

export default class extends React.Component {
    listCountry = [];

    constructor(props) {
        super(props);
        this.state = {
            isSave: false,
            isUpdate: false,
            isModalVisible: true,
            errors: [],
            isValidForm: false,
            listAddress: [],
            AddressName: '',
            TypeOfAddress: '',
            Street: '',
            StreetNo: '',
            BuildingFloorRoom: '',
            Block: '',
            ZipCode: '',
            City: '',
            Country: '',
            CountryText: '',
            County: '',
            State: '',
            RowNum: 0,
            AddressType: '',
            BPCode: ''
        }
    }

    componentWillMount() {
        this.loadAddress();
        this.loadCountries();
    }

    loadAddress() {
        if (this.props.isUpdate) {

            let addressItem = this.props.address;
            let typeToCreate = '';

            if (addressItem.AddressType == 'S' || addressItem.AddressType == 'Entrega' || addressItem.AddressType == 'bo_ShipTo')
                typeToCreate = 'Entrega';
            else
                typeToCreate = 'Cobrança';

            this.setState({
                ...addressItem,
                AddressType: typeToCreate
            });
        }
    }

    loadCountries() {
        var request = {
            entity: "Countries",
            //temporario, apenas para testear, listando apenas o pais Brasil
            conditionsUrl: "?$filter=Code eq 'BR'"
        }
        SisInstance.list(request)
            .then((data) => {
                this.serializeListCountries(data.value);
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    submitSuccess() {
        this.setState({ isValidForm: true });
        console.log("Submit Success!");
    }

    submitFailed() {
        this.setState({ isValidForm: false });
        console.log("Submit Faield!");
    }

    save() {
        this.submitForm();
    }

    serializeListCountries(list) {
        list.map((item) => {
            this.listCountry.push({ value: item.Name, Code: item.Code });
        });
    }

    duplicateAddress(address, idTypeToCreate, typeToCreate) {

        let newListAddress = [];
        let address1 = {
            AddressName: typeToCreate,
            CardCode: this.state.CardCode,
            TypeOfAddress: this.state.TypeOfAddress,
            Street: this.state.Street,
            StreetNo: this.state.StreetNo,
            BuildingFloorRoom: this.state.BuildingFloorRoom,
            Block: this.state.Block,
            ZipCode: this.state.ZipCode,
            City: this.state.City,
            Country: this.state.Country,
            County: this.state.County,
            State: this.state.State,
            RowNum: parseInt(this.state.RowNum),
            AddressType: this.state.AddressType,
            BPCode: this.state.BPCode
        }
        newListAddress.push(address1);

        let address2 = {
            AddressName: address.AddressName,
            CardCode: address.CardCode,
            TypeOfAddress: address.TypeOfAddress,
            Street: address.Street,
            StreetNo: address.StreetNo,
            BuildingFloorRoom: address.BuildingFloorRoom,
            Block: address.Block,
            ZipCode: address.ZipCode,
            City: address.City,
            Country: address.Country,
            County: address.County,
            State: address.State,
            RowNum: parseInt(address.RowNum) + 1,
            AddressType: idTypeToCreate,
            BPCode: address.BPCode
        }
        newListAddress.push(address2);
        this.setState({
            listAddress: newListAddress
        }, () => this.closeModal());
    }

    submitForm() {
        let submitResults = this.addressForm.validate();
        let errors = [];

        submitResults.forEach(item => {
            errors.push({ field: item.fieldName, error: item.error });
        });
        this.setState({ errors: errors });

        let isValidForm = true;
        submitResults.map((item) => {
            if (item.isValid == false)
                isValidForm = false;
        });

        if (isValidForm) {
            this.onChangeTypeAddress(this.state.AddressType);
            this.setState({ isSave: true });

            if (!this.props.addressList || this.props.addressList.length == 0) {
                let typeToCreate = '';
                let idTypeToCreate = '';
                if (this.state.AddressType == 'S' || this.state.AddressType == 'bo_ShipTo') {
                    typeToCreate = 'Cobrança';
                    idTypeToCreate = 'B';
                }
                else {
                    typeToCreate = 'Entrega';
                    idTypeToCreate = 'S';
                }

                Alert.alert(
                    'Pergunta',
                    'Deseja cadastrar este endereço como: ' + typeToCreate,
                    [
                        {
                            text: 'Sim',
                            onPress: () => {
                                this.duplicateAddress(this.state, idTypeToCreate, typeToCreate)
                            }
                        },
                        {
                            text: 'Não',
                            onPress: () => this.closeModal(),
                            //style: 'cancel',
                        },
                    ],
                    { cancelable: false },
                );
            } else {
                this.closeModal();
            }
        }
    }

    onChangeCountry(countrySelected) {
        let obj = this.listCountry.find(x => x.value == countrySelected);
        if (obj)
            this.setState({ CountryText: countrySelected, Country: obj.Code });
    }

    onChangeTypeAddress(typeSelected) {
        let id = '';
        if (typeSelected == "Cobrança" || typeSelected == "B" || typeSelected == "bo_BillTo")
            id = 'B';
        else
            id = 'S';

        this.setState({ AddressType: id });
    }

    closeModal() {
        this.setState({
            isModalVisible: false,
        }, () => { this.props.closeModal(this.state) });
    }

    render() {
        return (
            <Container style={{ flex: 1 }}>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.closeModal()}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'novo endereço'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.save()}>
                            <Icon active name='content-save' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Right>
                </Header>
                <View style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={[styles.page]}>
                        <Form
                            ref={(ref) => this.addressForm = ref}
                            validate={true}
                            submit={this.submitSuccess.bind(this)}
                            failed={this.submitFailed.bind(this)}
                            errors={this.state.errors}>

                            <View style={{ marginTop: 50 }}>
                                <Field
                                    autoCapitalize="none"
                                    textColor={baseColor}
                                    tintColor={baseColor}
                                    baseColor={baseColor}
                                    label="Nome do Endereço"
                                    secureTextEntry={false}
                                    required
                                    component={InputField}
                                    validations={[required, minLength(1), maxLength(50)]}
                                    name="AddressName"
                                    value={this.state.AddressName}
                                    onChangeText={(val) => this.setState({ AddressName: val })}
                                    customStyle={{ width: 100 }}
                                    errors={this.state.errors}
                                />
                            </View>

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Tipo de Endereço (Rua, Avenida)"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="TypeOfAddress"
                                value={this.state.TypeOfAddress}
                                onChangeText={(val) => this.setState({ TypeOfAddress: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Nome da Rua"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="Street"
                                value={this.state.Street}
                                onChangeText={(val) => this.setState({ Street: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Número"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="StreetNo"
                                value={this.state.StreetNo}
                                onChangeText={(val) => this.setState({ StreetNo: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Complemento"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(16)]}
                                name="BuildingFloorRoom"
                                value={this.state.BuildingFloorRoom}
                                onChangeText={(val) => this.setState({ BuildingFloorRoom: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Bairro"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="Block"
                                value={this.state.Block}
                                onChangeText={(val) => this.setState({ Block: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="CEP"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(20)]}
                                name="ZipCode"
                                value={this.state.ZipCode}
                                onChangeText={(val) => this.setState({ ZipCode: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Cidade"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="City"
                                value={this.state.City}
                                onChangeText={(val) => this.setState({ City: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Cidade"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(100)]}
                                name="County"
                                value={this.state.County}
                                onChangeText={(val) => this.setState({ County: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="characters"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Estado"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(2), maxLength(3)]}
                                name="State"
                                value={this.state.State}
                                onChangeText={(val) => this.setState({ State: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                baseColor={baseColor}
                                textColor={baseColor}
                                selectedItemColor={baseColor}
                                containerStyle={{ color: "#333" }}
                                label="Tipo de endereço"
                                required
                                validations={[required]}
                                component={InputDropdown}
                                name="AddressType"
                                value={this.state.AddressType}
                                onChangeText={(val) => this.onChangeTypeAddress(val)}
                                errors={this.state.errors}
                                data={addressTypeList}
                            />

                            <View style={{ marginBottom: 100 }}>
                                <Field
                                    baseColor={baseColor}
                                    textColor={baseColor}
                                    selectedItemColor={baseColor}
                                    containerStyle={{ color: "#333" }}
                                    label='Pais'
                                    required
                                    validations={[required]}
                                    component={InputDropdown}
                                    name="Country"
                                    value={this.state.Country}
                                    onChangeText={(val) => this.onChangeCountry(val)}
                                    errors={this.state.errors}
                                    data={this.listCountry}
                                />
                            </View>
                        </Form>
                    </ScrollView>
                </View>
            </Container>
        )
    }
}