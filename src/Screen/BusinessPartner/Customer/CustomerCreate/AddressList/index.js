//Libs
import React from "react";
import { Text, View, Content } from "native-base";
import { FlatList, TouchableOpacity, Modal, Alert, ScrollView } from 'react-native';
import AddressCreate from './AddressCreate';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

//Services
import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";

export default class extends React.Component {
    listErrors = [];

    constructor(props) {
        super(props);
        this.state = {
            //listAddress: [{ id: 1, CardCode: 'a', TypeOfAddress: 'a', BuildingFloorRoom: 'a', County: 'a', RowNum: 'a', AddressType: 'a', AddressName: 'Address 1', Street: 'Rua Dom Bosco', StreetNo: 10, Block: 'Bosque', ZipCode: '12333-232', City: 'São José dos Campos', State: 'SP', Country: 'Brasil' }, { id: 2, CardCode: 'b', TypeOfAddress: 'b', BuildingFloorRoom: 'b', County: 'b', RowNum: 'b', AddressType: 'a', AddressName: 'Address 2', Street: 'Rua Dom Bosco', StreetNo: 10, Block: 'Bosque', ZipCode: '12333-232', City: 'São José dos Campos', State: 'SP', Country: 'Brasil' }],
            listAddress: [],
            isModalVisible: false,
            addressEdit: {},
            isUpdate: false,
            indexUpdate: 0,
            isFetching: true
        }
    }

    validForm() {
        this.listErrors = [];
        return new Promise((resolve, reject) => {
            //reject('submit invalid');

            let containsAddressShipTo = false;//Entrega
            let containsAddressBillTo = false;//Cobranca
            this.state.listAddress.map((item) => {
                if (item.AddressType == 'B' || item.AddressType == 'bo_BillTo' || item.AddressType == 'Cobrança')
                    containsAddressBillTo = true;
                if (item.AddressType == 'S' || item.AddressType == 'bo_ShipTo' || item.AddressType == 'Entrega')
                    containsAddressShipTo = true;
            });

            let response = false;
            response = containsAddressBillTo && containsAddressShipTo ? true : false;

            if (!containsAddressBillTo)
                this.listErrors.push('O endereço de cobrança é obrigatório.');
            if (!containsAddressShipTo)
                this.listErrors.push('O endereço de entrega é obrigatório.');

            resolve(response);
        })
    }

    goToState(state) {
        this.props.hideViewStep();
        setTimeout(() => {
            NavigationService.navigate(state)
        }, 10)
    }

    closeModal(modalState) {
        if (modalState.isSave)
            this.serializeAddress(modalState);
        this.setState({
            isModalVisible: modalState.isModalVisible,
        });
    }

    serializeAddress(address) {
        let newAddress = null;
        let tempList = [];
        let maxRowNum = 0;

        if (this.state.listAddress && this.state.listAddress.length > 0) {
            maxRowNum = this.state.listAddress.reduce(function (a, b) {
                return Math.max(a.RowNum, b.RowNum);
            });
            maxRowNum = parseInt(maxRowNum) + 1;
        }


        if (address.listAddress.length > 1) {
            address.listAddress.map((item) => {
                let tempAddress = {
                    AddressName: item.AddressName,
                    //CardCode: item.CardCode,
                    TypeOfAddress: item.TypeOfAddress,
                    Street: item.Street,
                    StreetNo: item.StreetNo,
                    BuildingFloorRoom: item.BuildingFloorRoom,
                    Block: item.Block,
                    ZipCode: item.ZipCode,
                    City: item.City,
                    Country: item.Country,
                    County: item.County,
                    State: item.State,
                    AddressType: item.AddressType,
                    //RowNum: item.RowNum,
                    BPCode: this.props.cardCode
                }
                tempList.push(tempAddress);
            });

        } else {
            newAddress = {
                AddressName: address.AddressName,
                //CardCode: address.CardCode,
                TypeOfAddress: address.TypeOfAddress,
                Street: address.Street,
                StreetNo: address.StreetNo,
                BuildingFloorRoom: address.BuildingFloorRoom,
                Block: address.Block,
                ZipCode: address.ZipCode,
                City: address.City,
                Country: address.Country,
                County: address.County,
                State: address.State,
                //RowNum: maxRowNum,
                AddressType: address.AddressType,
                BPCode: this.props.cardCode
            }
            tempList.push(newAddress);
        }

        if (this.props.cardCode)
            newAddress.RowNum = address.RowNum;

        if (this.state.isUpdate) {
            this.state.listAddress.splice(this.state.indexUpdate, 1);
            this.setState({ listAddress: this.state.listAddress });
        }


        tempList = [...this.state.listAddress, ...tempList];
        this.setState({ listAddress: tempList });
        this.props.setAddressList(tempList);
    }

    editAddress(address, index) {
        this.setState({ addressEdit: address, isUpdate: true, indexUpdate: index });
        setTimeout(() => {
            this.setState({ isModalVisible: true });
        }, 10)
    }

    openModal() {
        this.setState({ isModalVisible: true, isUpdate: false });
    }

    getAddressType = (addressType) => {
        if (addressType == 'S' || addressType == 'bo_ShipTo')
            return 'Entrega';
        else if (addressType == 'B' || addressType == 'bo_BillTo')
            return 'Cobrança';
    }

    removeAddress(index) {
        this.setState({ isFetching: true });
        Alert.alert(
            'Pergunta',
            'Deseja mesmo excluir este endereço?',
            [
                {
                    text: 'Sim',
                    onPress: () => { this.state.listAddress.splice(index, 1), this.setState({ isFetching: false }); }
                },
                {
                    text: 'Não',
                    //style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }

    renderAddressRow(address, index) {
        return (
            <TouchableOpacity onPress={() => this.editAddress(address, index)}>
                <View style={styles.listItemContainer}>
                    <View style={styles.detailViewContainer}>
                        <View style={styles.detailsViewContainerWrap}>
                            <View style={[styles.nameViewContainer, { flex: 3 }]}>
                                <Text>{address.AddressName} ({this.getAddressType(address.AddressType)})</Text>
                                <View style={styles.descriptionViewItem}>
                                    <Text style={styles.text}>{address.Street}, {address.StreetNo}, {address.City} - {address.State} </Text>
                                </View>
                            </View>
                            <View style={styles.viewButtonRemove}>
                                <TouchableOpacity onPress={() => this.removeAddress(index)} style={styles.buttonRemove}>
                                    <IconFontAwesome name={"trash-o"} style={styles.iconRemove}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {

        const errors = this.listErrors.map(function (item) {
            return <Text>{item}</Text>;
        });

        return (
            <Content style={styles.page}>
                <ScrollView>
                <Modal visible={this.state.isModalVisible} presentationStyle={"fullScreen"}>
                    <AddressCreate closeModal={this.closeModal.bind(this)} address={this.state.addressEdit} isUpdate={this.state.isUpdate} addressList={this.state.listAddress} />
                </Modal>
                <View style={styles.viewContainer}>
                    {
                        this.listErrors.length > 0 ?
                            <View style={{ marginBottom: 50 }}>
                                <Text style={{ color: 'red' }}>Incosistências:</Text>
                                {errors}
                            </View> : null
                    }
                    <FlatList
                        refreshing={this.state.isFetching}
                        //onRefresh={this.onRefresh()}
                        keyboardShouldPersistTaps={'handled'}
                        data={this.state.listAddress}
                        //extraData={this.state.orderBy}
                        renderItem={({ item, index }) => this.renderAddressRow(item, index)}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReachedThreshold={5}
                    />
                </View>
                </ScrollView>
            </Content>
        )
    }
}