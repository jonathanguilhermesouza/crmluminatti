//Packages
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, ScrollView } from 'react-native';
import { Form, Field } from 'react-native-validate-form';

//Components
import InputField from '@Component/Input/InputField';
import InputDropdown from '@Component/Input/InputDropdown';

//Services

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';

const required = value => (value ? undefined : "Obrigatório");
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const addressTypeList = [{ value: 'Cobrança', id: 'B' }, { value: 'Entrega', id: 'S' }];
const baseColor = "#333";

export default class extends React.Component {
    listAddress = [];

    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: true,
            errors: [],
            isValidForm: false,
            isSave: false,
            isUpdate: false,

            Address: '',
            TaxId0: '',
            TaxId1: '',
            TaxId2: '',
            TaxId3: '',
            TaxId4: '',
            TaxId5: '',
            TaxId6: '',
            TaxId7: '',
            TaxId8: '',
            CNAECode: '',
            AddrType: '',
            BPCode: ''
        }
    }

    componentWillMount() {
        this.loadTaxInformation();
        this.loadListAddress();
    }

    loadListAddress() {
        this.props.listOfAddress.map((item) => {
            this.listAddress.push(
                { value: `${item.AddressName}` }
            );
        });
    }

    loadTaxInformation() {
        if (this.props.isUpdate) {
            let taxInformationItem = this.props.taxInformation;
            let typeToCreate = '';

            if (taxInformationItem.AddrType == 'S' || typeToCreate == 'Entrega')
                typeToCreate = 'Entrega';
            else
                typeToCreate = 'Cobrança';

            this.setState({
                ...taxInformationItem,
                AddrType: typeToCreate
            });
        }
    }

    submitSuccess() {
        this.setState({ isValidForm: true });
        console.log("Submit Success!");
    }

    submitFailed() {
        this.setState({ isValidForm: false });
        console.log("Submit Faield!");
    }

    save() {
        this.submitForm();
    }

    submitForm() {
        let submitResults = this.taxInformationForm.validate();
        let errors = [];

        submitResults.forEach(item => {
            errors.push({ field: item.fieldName, error: item.error });
        });
        this.setState({ errors: errors });

        let isValidForm = true;
        submitResults.map((item) => {
            if (item.isValid == false)
                isValidForm = false;
        });

        if (isValidForm) {
            this.onChangeTypeAddress(this.state.AddrType);
            this.setState({ isSave: true }, () => this.closeModal());
        }
    }

    closeModal() {
        this.setState({
            isModalVisible: false,
        }, () => { this.props.closeModal(this.state) });
    }

    onChangeTypeAddress(typeSelected) {
        let id = '';
        if (typeSelected == "Cobrança" || typeSelected == "B")
            id = 'B';
        else
            id = 'S';

        this.setState({ AddrType: id });
    }

    render() {
        return (
            <Container style={{ flex: 1 }}>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.closeModal()}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'informacão fiscal'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.save()}>
                            <Icon active name='content-save' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Right>
                </Header>
                <View style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={[styles.page]}>

                        <Form
                            ref={(ref) => this.taxInformationForm = ref}
                            validate={true}
                            submit={this.submitSuccess.bind(this)}
                            failed={this.submitFailed.bind(this)}
                            errors={this.state.errors}>

                            <View style={{ marginTop: 50 }}>
                                <Field
                                    baseColor={baseColor}
                                    textColor={baseColor}
                                    selectedItemColor={baseColor}
                                    containerStyle={{ color: "#333" }}
                                    label="Nome do Endereço"
                                    required
                                    validations={[required]}
                                    component={InputDropdown}
                                    name="Address"
                                    value={this.state.Address}
                                    onChangeText={(val) => this.setState({ Address: val })}
                                    errors={this.state.errors}
                                    data={this.listAddress}
                                />

                            </View>

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="CNPJ"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId0"
                                value={this.state.TaxId0}
                                onChangeText={(val) => this.setState({ TaxId0: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Inscrição Estadual"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId1"
                                value={this.state.TaxId1}
                                onChangeText={(val) => this.setState({ TaxId1: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Inscrição Estadual Substituto Tributário"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId2"
                                value={this.state.TaxId2}
                                onChangeText={(val) => this.setState({ TaxId2: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Inscrição Municipal"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId3"
                                value={this.state.TaxId3}
                                onChangeText={(val) => this.setState({ TaxId3: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="CPF"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId4"
                                value={this.state.TaxId4}
                                onChangeText={(val) => this.setState({ TaxId4: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="ID de estrangeiro"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId5"
                                value={this.state.TaxId5}
                                onChangeText={(val) => this.setState({ TaxId5: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Descrição estrangeira"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId6"
                                value={this.state.TaxId6}
                                onChangeText={(val) => this.setState({ TaxId6: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Inscrição INSS"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId7"
                                value={this.state.TaxId7}
                                onChangeText={(val) => this.setState({ TaxId7: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Suframa"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="TaxId8"
                                value={this.state.TaxId8}
                                onChangeText={(val) => this.setState({ TaxId8: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="CNAE"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(11)]}
                                name="CNAECode"
                                value={this.state.CNAECode}
                                onChangeText={(val) => this.setState({ CNAECode: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <View style={{ marginBottom: 100 }}>
                                <Field
                                    baseColor={baseColor}
                                    textColor={baseColor}
                                    selectedItemColor={baseColor}
                                    containerStyle={{ color: "#333" }}
                                    label="Tipo de endereço"
                                    required
                                    validations={[required]}
                                    component={InputDropdown}
                                    name="AddrType"
                                    value={this.state.AddrType}
                                    onChangeText={(val) => this.onChangeTypeAddress(val)}
                                    errors={this.state.errors}
                                    data={addressTypeList}
                                />
                            </View>
                        </Form>
                    </ScrollView>
                </View>
            </Container>
        )
    }
}