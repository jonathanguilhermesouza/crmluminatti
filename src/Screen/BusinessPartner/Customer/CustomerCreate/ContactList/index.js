//Libs
import React from "react";
import { Text, View, Content } from "native-base";
import { FlatList, TouchableOpacity, Modal, Alert, ScrollView } from 'react-native';
import ContactCreate from './ContactCreate';
import Moment from 'moment';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

//Services
import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            /*listContact: [
                {
                    CardCode: '1',
                    Name: 'Jonathan',
                    Position: 'Programmer',
                    Address: 'Travessa dos Galdinos',
                    Phone1: '12',
                    Phone2: '981559842',
                    MobilePhone: '12981559842',
                    E_Mail: 'jonathan@jojo.com',
                    Remarks1: 'bla',
                    Remarks2: 'bla',
                    BirthDate: '05/11/1991',
                    Gender: 'Masculimo',
                    Profession: 'Programmer',
                    Title: 'Sr',
                    Active: 'Sim',
                    FirstName: 'Jonathan',
                    MiddleName: 'Guilherme',
                    LastName: 'Santos'
                },
                {
                    CardCode: '2',
                    Name: 'Guilherme',
                    Position: 'Programmer',
                    Address: 'Travessa dos Galdinos',
                    Phone1: '12',
                    Phone2: '981559842',
                    MobilePhone: '12981559842',
                    E_Mail: 'jonathan@jojo.com',
                    Remarks1: 'bla',
                    Remarks2: 'bla',
                    BirthDate: '05/11/1991',
                    Gender: 'Masculimo',
                    Profession: 'Programmer',
                    Title: 'Sr',
                    Active: 'Sim',
                    FirstName: 'Junio',
                    MiddleName: 'Melo',
                    LastName: 'Santos'
                }
            ],*/
            listContact: [],
            isModalVisible: false,
            contactEdit: {},
            isUpdate: false,
            indexUpdate: 0,
            isFetching: false
        }
    }

    goToState(state) {
        this.props.hideViewStep();
        setTimeout(() => {
            NavigationService.navigate(state)
        }, 10)
    }

    validForm() {
        return new Promise((resolve, reject) => {
            //reject('submit invalid');
            resolve(true);
        })
    }

    closeModal(modalState) {
        if (modalState.isSave)
            this.serializeContactItem(modalState);
        this.setState({
            isModalVisible: modalState.isModalVisible,
        });
    }

    serializeContactItem(contactItem) {
        Moment.locale('en');
        let newContactItem = {
            CardCode: contactItem.CardCode,
            Name: contactItem.Name,
            Position: contactItem.Position,
            Address: contactItem.Address,
            Phone1: contactItem.Phone1,
            Phone2: contactItem.Phone2,
            MobilePhone: contactItem.MobilePhone,
            E_Mail: contactItem.E_Mail,
            Remarks1: contactItem.Remarks1,
            Remarks2: contactItem.Remarks2,
            DateOfBirth: Moment(contactItem.DateOfBirth).format('YYYY-MM-DD'),
            Gender: contactItem.Gender,
            Profession: contactItem.Profession,
            Title: contactItem.Title,
            Active: contactItem.Active,
            FirstName: contactItem.FirstName,
            MiddleName: contactItem.MiddleName,
            LastName: contactItem.LastName,
            InternalCode: contactItem.InternalCode
        }

        if (this.state.isUpdate) {
            this.state.listContact.splice(this.state.indexUpdate, 1);
            this.setState({ listContact: this.state.listContact });
        }

        let tempList = [];
        newContactItem.DateOfBirth = contactItem.DateOfBirth;
        tempList.push(newContactItem);
        tempList = [...this.state.listContact, ...tempList];
        this.setState({ listContact: tempList });
        this.props.setContactList(tempList);
    }

    editContact(contact, index) {
        this.setState({ contactEdit: contact, isUpdate: true, indexUpdate: index });
        setTimeout(() => {
            this.setState({ isModalVisible: true });
        }, 10)
    }

    openModal() {
        this.setState({ isModalVisible: true });
    }

    removeContact(index) {
        this.setState({ isFetching: true });
        Alert.alert(
            'Pergunta',
            'Deseja mesmo excluir este contato?',
            [
                {
                    text: 'Sim',
                    onPress: () => { this.state.listContact.splice(index, 1), this.setState({ isFetching: false }); }
                },
                {
                    text: 'Não',
                    //style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }

    renderRow(item, index) {
        return (
            <TouchableOpacity onPress={() => this.editContact(item, index)}>
                <View style={styles.listItemContainer}>
                    <View style={styles.detailViewContainer}>
                        <View style={styles.detailsViewContainerWrap}>
                            <View style={[styles.nameViewContainer, { flex: 3 }]}>
                                <Text>{item.FirstName} {item.LastName}</Text>
                                <View style={styles.descriptionViewItem}>
                                    <Text style={styles.text}>{item.Profession}</Text>
                                </View>
                            </View>
                            <View style={styles.viewButtonRemove}>
                                <TouchableOpacity onPress={() => this.removeContact(index)} style={styles.buttonRemove}>
                                    <IconFontAwesome name={"trash-o"} style={styles.iconRemove}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <Content style={styles.page}>
                <ScrollView>
                    <Modal visible={this.state.isModalVisible} presentationStyle={"fullScreen"}>
                        <ContactCreate closeModal={this.closeModal.bind(this)} contact={this.state.contactEdit} isUpdate={this.state.isUpdate} />
                    </Modal>
                    <View style={styles.viewContainer}>
                        <FlatList
                            refreshing={this.state.isFetching}
                            //onRefresh={this.onRefresh()}
                            keyboardShouldPersistTaps={'handled'}
                            data={this.state.listContact}
                            //extraData={this.state.orderBy}
                            renderItem={({ item, index }) => this.renderRow(item, index)}
                            keyExtractor={(item, index) => index.toString()}
                            onEndReachedThreshold={5}
                        />
                    </View>
                </ScrollView>
            </Content>
        )
    }
}