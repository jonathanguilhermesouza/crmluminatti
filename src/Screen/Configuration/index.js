
import {
    Container,
    Text,
    View,
  } from "native-base";
import React from "react";
import { Header, Button, Icon, Left, Right, Body} from 'native-base';
import {StatusBar, StyleSheet, Image} from 'react-native';

import GlobalStyle from '@Theme/GlobalStyle';
import NavigationService from '@Service/Navigation';
import styles from "./styles";

export default class extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
      return(
        <Container>
            <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                <StatusBar barStyle="light-content" backgroundColor="#999"/>
                <Left style={{flex: 1}}>
                    <Button transparent onPress={() => NavigationService.openDrawer()}>
                        <Icon active name='menu' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                </Left>
                <Body style={{flex: 1}}>
                    <Text style={[GlobalStyle.actionBarText,{alignSelf:'center'}]}>{'Dashboard'.toUpperCase()}</Text>
                </Body>
                <Right style={{flex: 1}}>
                    {/*<Button transparent onPress={() => this.onToggleInputSearch()}>
                    <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                    <Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
                    <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
                    </Button>*/}
                </Right>
            </Header>
            <View style={styles.viewContainer}>
                <Text style={styles.welcomeText}>Configuration</Text>
            </View>
        </Container>
      ) 
    }
  }