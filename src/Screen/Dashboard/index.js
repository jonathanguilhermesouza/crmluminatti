
import {
    Container,
    Text,
    View,
} from "native-base";
import React from "react";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, StyleSheet, Image, Dimensions } from 'react-native';

import GlobalStyle from '@Theme/GlobalStyle';
import DbConnectionService from '@Service/DbConnectionService';
import BusinessPlaceService from '@Service/BusinessPlaceService';
import NavigationService from '@Service/Navigation';
import ReactArt from 'react-art';
import styles from "./styles";

import 'reflect-metadata';
import { createConnection, getRepository } from "typeorm";
import { Cliente } from '@Database/Entity/Cliente';
import { BusinessPlace } from '@Database/Entity/BusinessPlace';

const deviceWidth = Dimensions.get("window").width;

var InstanceDbConnectionService = new DbConnectionService();
var InstanceBusinessPlaceService = new BusinessPlaceService();
export default class extends React.Component {
    constructor(props) {
        super(props);
        //this.createConnectionDB();
    }

    sincronizar() {

        var request = {
            conditionsUrl: "?$select=BPLID,BPLName,BPLNameForeign&$orderby=BPLName"
        }
        InstanceBusinessPlaceService.list(request)
            .then((data) => {
                //alert(JSON.stringify(data.value));
                this.insertBusinessPlace(data.value);
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            }).finally(() => {

            });
    }

    insertBusinessPlace(listBusinessPlace) {

        const businessPlaceRepository = getRepository(BusinessPlace);

        listBusinessPlace.map((place) => {

            const businessPlace = new BusinessPlace();
            businessPlace.BPLID = place.BPLID;
            businessPlace.BPLName = place.BPLName;
            businessPlace.BPLNameForeign = place.BPLNameForeign;

            //alert(JSON.stringify(businessPlace));
            //alert(JSON.stringify(businessPlace));

            businessPlaceRepository.save(businessPlace)
                .then((response) => { alert('deu certo') })
                .catch((error) => { alert(JSON.stringify(error)) });

        });
        //const clienteRepository = getRepository(Cliente);

        //clienteRepository.findOne()
        //  .then(response => alert(JSON.stringify(response)));

        const bpRepository = getRepository(BusinessPlace);

        bpRepository.findOne()
            .then(response => alert(JSON.stringify(response)));

    }

    createConnectionDB() {

        InstanceDbConnectionService.connection()
            .then((response) => {

                this.sincronizar();

                //const clienteRepository = getRepository(Cliente);

                //clienteRepository.findOne()
                //.then(response => alert(JSON.stringify(response)));


            })
            .catch((error) => { alert(JSON.stringify(error)) });
    }

    render() {
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.openDrawer()}>
                            <Icon active name='menu' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'Dashboard'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {/*<Button transparent onPress={() => this.onToggleInputSearch()}>
                    <Icon active name='arrow-left' style={Style.textWhite} type="MaterialCommunityIcons" />
                    </Button>
                    <Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
                    <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
                    </Button>*/}
                    </Right>
                </Header>
                <View style={{
                    marginLeft: 10,
                    marginRight: 10
                }}>
                    {/* <View style={{
                        flexDirection: 'row'
                    }}>
                        <Button rounded style={{
                            marginTop: 30,
                            flex: 1,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: "#666"
                        }}>
                            <Text uppercase={true}>Atualizar Dashboard</Text>
                        </Button>
                    </View>*/}
                </View>
                {/*<View style={{
                    marginLeft: 10,
                    marginRight: 10
                }}>
                    <View style={{
                        flexDirection: 'row'
                    }}>
                        <Button rounded style={{
                            marginTop: 30,
                            flex: 1,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: "#666"
                        }}>
                            <Text uppercase={true}>Atualizar Dashboard</Text>
                        </Button>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "center", marginLeft: 50,
                        marginRight: 50, marginTop: 5
                    }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 10, color: "#333" }}>Últ. Atuali: 01/04/2019 16:37</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 10, alignSelf: "flex-end", color: "#333" }}>Período: 03/2019</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "center",
                        marginTop: 30
                    }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 15, color: "#333" }}>Pedidos</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 15, alignSelf: "flex-end", color: "#333" }}>R$ 3.609.434,90</Text>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: "row", justifyContent: "center"
                    }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 15, color: "#333" }}>Títulos Vencidos(todos)</Text>
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 15, alignSelf: "flex-end", color: "#333" }}>R$ 1.434.334,22</Text>
                        </View>

                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 40 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 18, alignSelf: "center", color: "#333" }}>Indicadores</Text>
                            <View style={{ flexDirection: "row", marginTop: 15 }}>
                                <View style={{ flex: 2, height: 200 }}>
                                    <Text style={{ fontSize: 10, marginTop: 40, color: "#333" }}>Bloqueado</Text>
                                    <Text style={{ fontSize: 10, marginTop: 35, color: "#333" }}>Faturado</Text>
                                    <Text style={{ fontSize: 10, marginTop: 40, color: "#333" }}>Não sincronizado</Text>
                                </View>
                                <View style={{ flex: 8 }}>
                                    <Image style={{ width: deviceWidth - 100, height: 200 }} source={require("@Asset/images/dashboard/chart-1.png")} />

                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={{ fontSize: 10, marginLeft: 15, color: "#333" }}>0</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 45, color: "#333" }}>500</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 45, color: "#333" }}>1000</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 45, color: "#333" }}>1500</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 45, color: "#333" }}>2000</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "center", marginTop: 40 }}>
                        <View style={{ flex: 1 }}>
                            <Text style={{ fontSize: 18, alignSelf: "center", color: "#333" }}>Vendas Mensais</Text>
                            <View style={{ flexDirection: "row", marginTop: 15 }}>
                                <View style={{ flex: 2, height: 200 }}>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>3500</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>3000</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>2500</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>2000</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>1500</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>1000</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>500</Text>
                                    <Text style={{ fontSize: 10, marginTop: 14 }}>0</Text>
                                </View>
                                <View style={{ flex: 8 }}>
                                    <Image style={{ width: deviceWidth - 100, height: 200 }} source={require("@Asset/images/dashboard/chart-2.png")} />

                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={{ fontSize: 10, marginLeft: 16, color: "#333" }}>10/2018</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 13, color: "#333" }}>11/2018</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 11, color: "#333" }}>12/2018</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 10, color: "#333" }}>01/2018</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 9, color: "#333" }}>02/2018</Text>
                                        <Text style={{ fontSize: 10, marginLeft: 8, color: "#333" }}>03/2018</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </View>
                </View>*/}
                <View style={styles.viewContainer}>
                    <View style={styles.TrapezoidStyle} />
                    <Image source={require('@Asset/images/logo-dark.png')} resizeMode={'cover'} style={styles.logo} />
                    <Text style={styles.welcomeText}>Seja muito bem-vindo ao CRM Luminatti</Text>
                </View>
            </Container>
        )
    }
}