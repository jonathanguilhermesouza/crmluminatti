const React = require("react-native");
const { Dimensions, Platform, StatusBar } = React;
const CommonColor = require("@Theme/Variables/CommonColor");
const deviceWidth = Dimensions.get('window').width; //full width
const deviceHeight = Dimensions.get('window').height; //full height
const lightColor = "#fff";

export default {
  container: {
    flex: 2,
    flexDirection: "row",
    alignSelf: "center",
    /*marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight*/
  },
  background: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: "transparent"
  },
  backgroundImage: {
    width: deviceWidth, 
    height: Platform.OS === 'ios' ? deviceHeight : deviceHeight + StatusBar.currentHeight
  },
  containerLogo: {
    flex: 1,
    minHeight: 50,
    maxHeight: 300,
    flexDirection: "row",
    alignSelf: "center",
   /* marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,*/
  },
  logo: {
    flex: 1,
    resizeMode: "contain",
    height: deviceHeight / 4,
    alignSelf: "center"
  },
  otherLinksContainer: {
    flexDirection: "row"
  },
  helpBtns: {
    opacity: 0.9,
    fontWeight: "bold",
    color: "#fff",
    fontSize: Platform.OS === "android" ? 12 : 12
  },
  form: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  formErrorIcon: {
    color: lightColor,
    marginTop: 5,
    right: 10
  },
  formErrorText1: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: CommonColor.getDangerColor,
    textAlign: "right",
    top: -10
  },
  formErrorText2: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: "transparent",
    textAlign: "right",
    top: -10
  },
  loginBtn: {
    marginTop: 7,
    height: 50
  },
  input: {
    paddingLeft: 10,
    color: lightColor
  },
  buttonLogin:{
    padding: 10,
    backgroundColor: lightColor,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:80
  },
  switchContainer: {
      flex: 1, 
      marginTop: 30
  },
  switch: {
    transform: [{ scaleX:  Platform.OS === 'ios' ? 1 : 1.4 }, { scaleY:  Platform.OS === 'ios' ? 1 : 1.4 }]
  },
  switchLabel: {
    color: lightColor, 
    marginLeft: 15
  }
};
