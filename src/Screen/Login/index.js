import React, { Component } from "react";
import { Image, StatusBar, ImageBackground, TouchableOpacity, AsyncStorage, ActivityIndicator, Modal } from "react-native";
import {
  Container,
  Content,
  Text,
  View,
  Toast,
  ActionSheet,
  Switch,
  Left,
  Right,
  Button
} from "native-base";
import { Form, Field } from 'react-native-validate-form';
import { reduxForm } from "redux-form";
//import Spinner from 'react-native-spinkit';

//Componentes
import InputField from '@Component/Input/InputField';
import InputDropdown from '@Component/Input/InputDropdown';
import PasswordInputText from '@Component/Input/PasswordInputText';

//Servicos
import AccountService from '@Service/AccountService';
import NetworkService from '@Service/NetworkService';

//Estilos
import styles from "./styles";
import CommonColor from '@Theme/Variables/CommonColor';
const lightColor = CommonColor.getDefaultLightTextColor;
const thumbTintColor = CommonColor.themeColor;
const onTintColor = "#cccccc";

const logo = require("@Asset/images/logo.png");

const required = value => (value ? undefined : "Obrigatório");
/*const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
Funcoes de validacao, descomente caso necessário utilizar alguma
const maxLength15 = maxLength(15);
const minLength8 = minLength(8);
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const alphaNumeric = value => value && /[^a-zA-Z0-9 ]/i.test(value) ? "Deve ter apenas caracteres alfanuméricos" : undefined;
*/

let AccountServiceInstance = new AccountService();
class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',//'saul@luminattiled.com.br',
      password: '',//'123456',
      businessUnit: '',
      toggleRememberCredentials: true,
      toggleRememberLoginOffline: false,
      dateTimeLogin: new Date(),
      sessionId: '',
      nameIcon: 'visibility-off',
      isPasswordInvisible: true,
      errors: [],
      isInvalidForm: false,
      spinner: false
    }
  }

  componentWillMount() {
    Toast.toastInstance = null;
    ActionSheet.actionsheetInstance = null;
    this.getDataCredentials();
  }

  //#region Local Storage
  setItemStorage = async (basicDataUserLogged) => {
    try {
      await AsyncStorage.setItem('BASIC_DATA_USER_LOGGED', JSON.stringify(basicDataUserLogged));
    } catch (error) {
    }
  }

  removeDataCredentials = async () => {
    try {
      AsyncStorage.removeItem('BASIC_DATA_USER_LOGGED');
    } catch (error) {
      console.log(error);
    }
  }

  getDataCredentials = async () => {
    try {
      AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
        if (result !== null) {
          let basicDataUserLogged = JSON.parse(result);

          this.setState({
            username: basicDataUserLogged.username,
            password: basicDataUserLogged.password,
            businessUnit: basicDataUserLogged.businessUnit,
            sessionId: basicDataUserLogged.sessionId,
            dateTimeLogin: basicDataUserLogged.dateTimeLogin,
            toggleRememberCredentials: basicDataUserLogged.toggleRememberCredentials,
            toggleRememberLoginOffline: basicDataUserLogged.toggleRememberLoginOffline
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  saveInfoLoginStorage() {
    let basicDataUserLogged = {
      username: this.state.username,
      password: this.state.password,
      businessUnit: this.state.businessUnit,
      sessionId: this.state.sessionId,
      dateTimeLogin: this.state.dateTimeLogin,
      toggleRememberCredentials: this.state.toggleRememberCredentials,
      toggleRememberLoginOffline: this.state.toggleRememberLoginOffline
    }
    if (this.state.toggleRememberCredentials)
      this.setItemStorage(basicDataUserLogged);
    else
      this.removeDataCredentials();
  }
  //#endregion

  //#region Login
  login() {

    this.setState({ spinner: true });

    var loginRequest = {
      CompanyDB: "APPTEST",
      UserName: this.state.username, //"manager",
      Password: this.state.password //"SpsHabil_9"
    }

    AccountServiceInstance.login(loginRequest)
      .then((data) => {
        this.setState({ sessionId: data.SessionId });
        this.saveInfoLoginStorage();
        if (data.SessionId)
          this.props.navigation.navigate("BusinessPlaceView");
        //else
          //alert("Não foi possível acessar o aplicativo");
        console.log(data);
      })
      .catch((error) => {
        alert(JSON.stringify(error));
        console.log(error);
        //alert("Não foi possível acessar o aplicativo");
      }).finally(() => {
        this.setState({ spinner: false });
      });
  }

  loginOffline() {
    var loginRequest = {
      CompanyDB: "SBODEMOBR",
      UserName: this.state.username, //"manager",
      Password: this.state.password //"SpsHabil_9"
    }

    AccountServiceInstance.loginOffline(loginRequest)
      .then((success) => {
        if (success)

          this.props.navigation.navigate("Dashboard");
        else
          alert('Usuário ou/e senha iválido(s)');
      });
  }
  //#endregion Login

  showPassword() {
    let newState;
    if (this.state.isPasswordInvisible) {
      newState = {
        nameIcon: 'visibility',
        isPasswordInvisible: false
      }
    } else {
      newState = {
        nameIcon: 'visibility-off',
        isPasswordInvisible: true
      }
    }

    // set new state value
    this.setState(newState)
  }

  //#region Formulario
  submitForm() {
    let submitResults = this.loginForm.validate();
    let errors = [];

    submitResults.forEach(item => {
      errors.push({ field: item.fieldName, error: item.error });
    });
    this.setState({ errors: errors });
  }

  submitSuccess() {
    this.setState({ isInvalidForm: false });
    if (this.state.toggleRememberLoginOffline) {
      this.loginOffline();
      return;
    }
    NetworkService.isConnected()
      .then((isConnected) => {
        if (isConnected)
          this.login();
        else if (!this.state.toggleRememberLoginOffline)
          alert('Não é possível realizar o login online. Para acessar o aplicativo você deve marcar a opção "Login offline" na tela de login');
        else
          this.loginOffline();
      });
    console.log("Submit Success!");
  }

  submitFailed() {
    this.setState({ isInvalidForm: true });
    alert('Verifique os campos e tente novamente.');
    console.log("Submit Faield!");
  }
  //#endregion Formulario

  render() {
    let dataBusinessUnit = [{
      value: 'Filial 1',
    }, {
      value: 'Filial 2',
    }, {
      value: 'Filial 3',
    }];
    return (
      <Container>


        {/*<Modal
          animationType="none"
          transparent={true}
          visible={this.state.spinner}
         >
          <View style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        </Modal>*/}


        <ImageBackground source={require('@Asset/images/background-login.png')} style={styles.backgroundImage} >
          <StatusBar barStyle="light-content" backgroundColor="#999" />
          <Content contentContainerStyle={styles.background}>

            {/*<Spinner style={{marginBottom: 50}} isVisible={this.state.spinner} size={100} type={"Circle"} color={"#fff"}/>*/}
            <View style={styles.containerLogo}>
              <Image source={logo} style={styles.logo} />
            </View>
            <View style={styles.container}>
              <View style={styles.form}>
                <Form
                  ref={(ref) => this.loginForm = ref}
                  validate={true}
                  submit={this.submitSuccess.bind(this)}
                  failed={this.submitFailed.bind(this)}
                  errors={this.state.errors}
                >
                  {/*Usuário*/}
                  <View>


                    <Field
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="USUÁRIO"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[required]}
                      name="username"
                      value={this.state.username}
                      onChangeText={(val) => this.setState({ username: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />

                    {/*<TextField
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="USUÁRIO"
                      value={this.state.username}
                      secureTextEntry={false}
                      required={true}
                      onChangeText={(username) => this.setState({ username })}
                    />*/}

                  </View>

                  {/*Senha*/}
                  <View>
                    <Field
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="SENHA"
                      secureTextEntry={this.state.isPasswordInvisible}
                      required
                      component={PasswordInputText}
                      validations={[required]}
                      name="password"
                      value={this.state.password}
                      onChangeText={(password) => this.setState({ password })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                      nameIcon={this.state.nameIcon}
                      onPress={() => this.showPassword()}
                      iconColor={lightColor}
                    />
                  </View>

                  {/*Unidade de negócios*/}
                  {/*<Field
                    baseColor={lightColor}
                    textColor={lightColor}
                    selectedItemColor={"#000"}
                    containerStyle={{ color: lightColor }}
                    label='UNIDADE DE NEGÓCIOS'
                    required
                    validations={[required]}
                    component={InputDropdown}
                    name="businessUnit"
                    value={this.state.businessUnit}
                    onChangeText={(businessUnit) => this.setState({ businessUnit })}
                    errors={this.state.errors}
                    data={dataBusinessUnit}
                  />

                  {/* 
                  <Dropdown
                    baseColor="#fff"
                    textColor="#fff"
                    selectedItemColor="#000"
                    containerStyle={{ color: "#fff" }}
                    label='UNIDADE DE NEGÓCIOS'
                    data={dataBusinessUnit}
                    onChangeText={(businessUnit) => this.setState({ businessUnit })}
                  />
                  */}

                  <View style={styles.otherLinksContainer}>
                    <Left>
                      {/* <Button
                      small
                      transparent
                      style={{ alignSelf: "flex-start" }}
                      onPress={() => navigation.navigate("SignUp")}
                    >
                      <Text style={styles.helpBtns}>Cadastrar</Text>
                    </Button>*/}
                    </Left>
                    <Right>
                      <Button
                        small
                        transparent
                        style={{ alignSelf: "flex-end" }}
                        onPress={() => this.props.navigation.navigate("ForgotPassword")}
                      >
                        <Text style={styles.helpBtns}>Esqueci a senha</Text>
                      </Button>
                    </Right>
                  </View>
                </Form>

                {/*Botão Login*/}
                <TouchableOpacity style={styles.buttonLogin} onPress={this.submitForm.bind(this)} disabled={this.state.spinner}>
                  <Text style={{ color: CommonColor.defaultThemeTextColor }}>Entrar</Text>
                </TouchableOpacity>

                {/*Switch Container*/}
                <View style={styles.switchContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <Switch
                      thumbTintColor={this.state.toggleRememberCredentials ? thumbTintColor : CommonColor.getDefaultLightTextColor}
                      onTintColor={onTintColor}
                      style={styles.switch}
                      onValueChange={(value) => this.setState({ toggleRememberCredentials: value })}
                      value={this.state.toggleRememberCredentials}
                    />
                    <Text style={styles.switchLabel}>Lembrar usuário e senha</Text>
                  </View>

                  {/*<View style={{ flexDirection: "row", marginTop: 30 }}>
                    <Switch
                      thumbTintColor={this.state.toggleRememberLoginOffline ? thumbTintColor : CommonColor.getDefaultLightTextColor}
                      onTintColor={onTintColor}
                      style={styles.switch}
                      onValueChange={(value) => this.setState({ toggleRememberLoginOffline: value })}
                      value={this.state.toggleRememberLoginOffline}
                    />
                    <Text style={styles.switchLabel}>Login offline</Text>
                  </View>*/}
                </View>
              </View>
            </View>
          </Content>
        </ImageBackground>
      </Container>
    );
  }
}
const Login = reduxForm({
  form: "login"
})(LoginForm);
export default Login;
