import CommonColor from '@Theme/Variables/CommonColor';

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    marginTop: 40,
    backgroundColor: "#fff"
  }
}