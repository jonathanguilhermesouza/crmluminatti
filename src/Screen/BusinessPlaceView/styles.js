import CommonColor from '@Theme/Variables/CommonColor';

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    marginTop: 40,
    backgroundColor: "#fff"
  },
  headerLeft: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  headerRight: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  btnClose: {
    fontSize: 20,
    color: CommonColor.getThemeColor
  },
  headerLeftText: {
    fontSize: 20,
    fontWeight: "bold",
    color: CommonColor.defaultTextColor
  },
  viewDescription: {
    flexDirection: "row",
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  viewDescriptionText: {
    fontSize: 20,
    color: CommonColor.defaultTextColor
  },
  labelStyleRadioForm: {
    fontSize: 18,
    color: CommonColor.defaultTextColor
  },
  viewBtnSave: {
    flex: 1,
    flexDirection: "row"
  },
  btnSave: {
    fontSize: 20,
    color: "#fff"
  },
  btnSaveTouchableOpacity: {
    flex: 1,
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: "#25D366",
    alignSelf: "flex-end"
  }
}