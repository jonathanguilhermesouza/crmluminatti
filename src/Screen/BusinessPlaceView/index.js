//Components
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, TouchableOpacity, AsyncStorage } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

//Services
import NavigationService from '@Service/Navigation';
import BusinessPlaceService from '@Service/BusinessPlaceService';

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';
import CommonColor from '@Theme/Variables/CommonColor';

let BusinessPlaceServiceInstance = new BusinessPlaceService();
export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            //listBussinessPlace: [{label: 'Danuri-SC (Luminatti)', value: 1},{label: 'Danuri-SP (Orluce)', value: 2}],
            listBussinessPlace: [],
            initialSelectedBP: null
        }
    }

    componentDidMount() {
        this._sub = this.props.navigation.addListener('didFocus', () => (
            this.loadBusinessPlace()
        ))
    }

    loadBusinessPlace() {
        var request = {
            conditionsUrl: "?$select=BPLID,BPLName&$orderby=BPLName"
        }
        BusinessPlaceServiceInstance.list(request)
            .then((data) => {
                this.serializeListBussinessPlace(data.value);
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    serializeListBussinessPlace(listBusinessPlace) {

        let tempList = [];
        let i = 0;
        listBusinessPlace.map((place) => {
            tempList.push(
                {
                    label: place.BPLName,
                    value: place.BPLID,
                    position: i++
                }
            );
        });

        this.setState({
            listBussinessPlace: tempList
        }, () => {
            this.getBusinessPlaceSelectedStorage();
        });
    }

    //#region localstorage
    setBusinessPlaceSelectedStorage = async (businessPlaceSelected) => {
        try {
            await AsyncStorage.setItem('BUSINESS_PLACE_SELECTED', JSON.stringify(businessPlaceSelected));
            NavigationService.navigate("Dashboard")
        } catch (error) {
        }
    }

    removeBusinessPlaceSelectedStorage = async () => {
        try {
            AsyncStorage.removeItem('BUSINESS_PLACE_SELECTED');
        } catch (error) {
            console.log(error);
        }
    }

    getBusinessPlaceSelectedStorage = async () => {
        try {
            AsyncStorage.getItem('BUSINESS_PLACE_SELECTED', (err, result) => {

                if (result !== null) {
                    let businessPlaceSelected = JSON.parse(result);
                    let searchItemBp = this.state.listBussinessPlace.find(x => x.value == businessPlaceSelected.bplId);
                    if (searchItemBp) {
                        this.refs.radioForm.updateIsActiveIndex(searchItemBp.position);
                        this.setState({
                            initialSelectedBP: searchItemBp.position
                        });
                    }
                }
            });
        } catch (error) {
            console.log(error);
        }
    }
    //#endregion

    saveBusinessPlaceSelected() {
        let businessPlace = { bplId: this.state.initialSelectedBP };
        this.removeBusinessPlaceSelectedStorage();
        this.setBusinessPlaceSelectedStorage(businessPlace);
    }

    render() {
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.openDrawer()}>
                            <Icon active name='menu' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'Filiais'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                    </Right>
                </Header>
                <View style={styles.viewContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <Left style={styles.headerLeft}>
                            <Text style={styles.headerLeftText}>FILIAIS</Text>
                        </Left>
                        <Body style={{ flex: 1 }}></Body>
                        <Right style={styles.headerRight}>
                            <TouchableOpacity onPress={() => NavigationService.navigate("Dashboard")}>
                                <Text style={styles.btnClose}>Fechar</Text>
                            </TouchableOpacity>
                        </Right>
                    </View>
                    <View style={styles.viewDescription}>
                        <Text style={styles.viewDescriptionText}>Selecione a filial desejada</Text>
                    </View>
                    <View style={{ flex: 1, padding: 30 }}>
                        <RadioForm
                            ref={"radioForm"}
                            labelStyle={styles.labelStyleRadioForm}
                            buttonSize={30}
                            radio_props={this.state.listBussinessPlace}
                            initial={this.state.initialSelectedBP}
                            formHorizontal={false}
                            labelHorizontal={true}
                            buttonColor={CommonColor.getThemeColor}
                            selectedButtonColor={CommonColor.getThemeColor}
                            animation={true}
                            onPress={(value) => { this.setState({ initialSelectedBP: value }) }}
                        />
                    </View>
                    <View style={styles.viewBtnSave}>
                        <TouchableOpacity onPress={() => this.saveBusinessPlaceSelected()} style={styles.btnSaveTouchableOpacity}>
                            <Text style={styles.btnSave}>Salvar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Container>
        )
    }
}