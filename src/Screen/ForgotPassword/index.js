// @flow
import React, { Component } from "react";
import { StatusBar, TouchableOpacity } from "react-native";
import {
  Container,
  Text,
  Button,
  View,
  Toast,
} from "native-base";
import { Field, reduxForm } from "redux-form";
import styles from "./styles";
import { TextField } from 'react-native-material-textfield';
import CommonColor from '@Theme/Variables/CommonColor';

const required = value => (value ? undefined : "Obrigatório");
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Endereço de email inválido"
    : undefined;

class ForgotPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };
  }

  renderInput({ input, label, type, meta: { touched, error, warning } }) {
    return (
      <View>
        <TextField
          autoCapitalize="none"
          ref={c => (this.textInput = c)}
          textColor={CommonColor.getDefaultLightTextColor}
          tintColor={CommonColor.getDefaultLightTextColor}
          baseColor={CommonColor.getDefaultLightTextColor}
          label="INFORME SEU USUÁRIO"
          {...input}
        />
        {touched && error
          ? <Text style={styles.formErrorText1}>
            {error}
          </Text>
          : <Text style={styles.formErrorText2}>error here</Text>}
      </View>
    );
  }

  forgotPassword() {
    if (this.props.valid) {
      this.props.navigation.goBack();
    } else {
      Toast.show({
        text: "Informe um email válido",
        duration: 2500,
        position: "top",
        textStyle: { textAlign: "center" }
      });
    }
  }

  render() {
    return (
      <Container style={styles.background}>
        <StatusBar barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Text style={styles.forgotPasswordHeader}>
            Recuperação de senha
              </Text>
          <View style={styles.forgotPasswordContainer}>
            <Field name="email" component={this.renderInput} type="email" validate={[email, required]} />
            <Button block onPress={() => this.forgotPassword()} style={styles.recoverPassword}>
              <Text style={{ color: "#00aff0" }}>Recuperar</Text>
            </Button>
          </View>
        </View>
        <View style={styles.buttonFooter}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={[styles.buttonGoOut]}>
            <Text style={{ fontSize: 20, color: CommonColor.getDefaultLightTextColor }}>Voltar para login</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const ForgotPassword = reduxForm({
  form: "help"
})(ForgotPasswordForm);
export default ForgotPassword;
